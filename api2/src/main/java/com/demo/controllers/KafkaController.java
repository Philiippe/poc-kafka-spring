package com.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Deuxième API non utilisé qui affiche le message
 * envoyé par l'api 1
 */

@RestController
@RequestMapping(value = "/kafka-consumer")
public class KafkaController {

    @GetMapping(value = "/api2")
    public void sendMessageToKafkaTopic() {
    }
}
