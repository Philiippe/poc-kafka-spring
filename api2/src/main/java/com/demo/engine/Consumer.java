package com.demo.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Consommateur qui va récupérer les données et les afficher
 * @param message données à afficher
 * @throws IOException
 */

@Service
public class Consumer {

    @KafkaListener(topics = "poc-topic", groupId = "group_id")
    public void consume(String message) throws IOException {
        System.out.println(String.format("Message reçu sur l'API 2 -> '%s'", message));
    }
}
